import static org.junit.Assert.*;

import java.util.ArrayList;

import javax.json.JsonObject;

import org.junit.Test;

import webservice.UtisWebserviceImpl;


public class UtisWSTests {
        @Test
        public void WSSearchTest() {
             System.out.println("*** Test search service ***");
             UtisWebserviceImpl serviceIMPL = new UtisWebserviceImpl();
             ArrayList<String> l = new ArrayList();
             l.add("WORMS");
             ArrayList<JsonObject> check = serviceIMPL.search("anomalina", "included", l);
             assertNotNull(check);
             for(JsonObject a : check){
                 System.out.println(a.toString());
             }
        }
    }
