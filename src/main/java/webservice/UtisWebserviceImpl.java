package webservice;

import java.util.ArrayList;
import java.util.List;
import javax.json.JsonObject;
import org.apache.log4j.Logger;
import org.gfbio.formating.GFBioJsonString;
import org.gfbio.formating.GFBioResultSetEntry;
import org.gfbio.formating.GFBioTSFormatter;
import org.gfbio.interfaces.WSInterface.DomainNames;
import io.swagger.client.ApiException;
import io.swagger.client.api.DefaultApi;
import io.swagger.client.model.Response;
import io.swagger.client.model.Taxon;
import io.swagger.client.model.TnrMsg;

/**
 * Webservice implementation in the GFBio Terminology Service. This class implements all necessary
 * methods for the TS endpoints according to the WSInterface defined in the gfbioapi project.
 * 
 * @author vincentb <A HREF="mailto:vincentb@imp.fu-berlin.de">Vincent Bohlen</A>
 *
 */
public class UtisWebserviceImpl {

  private static final Logger LOGGER = Logger.getLogger(UtisWebserviceImpl.class);
  public final List<String> domains;

  private DefaultApi utisAPI;

  public UtisWebserviceImpl() {
    this.utisAPI = new DefaultApi();
    this.domains = new ArrayList<String>(1);
    this.domains.add(DomainNames.Biological.name());

    LOGGER.info(" a UTIS webservice is ready to use");

  }

  /**
   * 
   * @param query the search query e.g. "Anomalina", "quercus robur", etc.
   * @param match_type exact or included
   * @param services Lis of acronyms of the services to search in (pesi, col or worms)
   * @return
   */
  /**
   * Implements the search method of the WSInterface Searches for names and common names in the
   * database based on the match type "exact" or "included"
   * 
   * @return ArrayList<JsonObject>
   */
  public ArrayList<JsonObject> search(final String query, final String match_type,
      List<String> services) {
    LOGGER.info("search query " + query + " matchtype " + match_type);
    StringBuilder serviceBuilder = new StringBuilder();
    for (String service : services) {
      if (service.toLowerCase().equals("col")) {
        serviceBuilder.append("bgbm-cdm-server[col]");
      } else {
        serviceBuilder.append(service.toLowerCase());
      }
      if (services.indexOf(service) != services.size()) {
        serviceBuilder.append(",");
      }
    }
    GFBioTSFormatter formatter = new GFBioTSFormatter();
    TnrMsg trans = null;
    try {
      if (match_type.equals("exact")) {
        trans = this.utisAPI.search(query, serviceBuilder.toString(), "scientificNameExact", false,
            null);
      } else if (match_type.equals("included")) {
        trans = this.utisAPI.search(query, serviceBuilder.toString(), "scientificNameLike", false,
            null);
      }
    } catch (ApiException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    if (trans != null) {
      for (Response response : trans.getQuery().get(0).getResponse()) {
        GFBioResultSetEntry entry = new GFBioResultSetEntry();
        Taxon tax = response.getTaxon();
        checkAndAdd(DomainNames.Biological.name(), "domain", entry);
        checkAndAdd(tax.getClassification().getKingdom(), "kingdom", entry);
        checkAndAdd(tax.getTaxonName().getFullName(), "label", entry);
        checkAndAdd(tax.getTaxonName().getRank(), "rank", entry);
        checkAndAdd(tax.getTaxonomicStatus(), "status", entry);
        checkAndAdd(response.getChecklist(), "sourceTerminology", entry);
        checkAndAdd(tax.getUrl(), "uri", entry);
        checkAndAdd(tax.getIdentifier(), "externalID", entry);

        formatter.addEntry(entry);
      }
    }
    LOGGER.info("search query processed " + query + " matchtype " + match_type);
    return formatter.getResultsAsJson();
  }

  private void checkAndAdd(String value, String attributelabel, GFBioResultSetEntry entry) {
    if (value != null) {
      entry.addJsonMember(attributelabel, new GFBioJsonString(value));
    }
  }
}
