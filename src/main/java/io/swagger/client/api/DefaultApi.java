package io.swagger.client.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sun.jersey.multipart.FormDataMultiPart;

import io.swagger.client.ApiException;
import io.swagger.client.ApiInvoker;
import io.swagger.client.model.ServiceProviderInfo;
import io.swagger.client.model.TnrMsg;

public class DefaultApi {
	String basePath = "https://cybertaxonomy.eu/eubon-utis";
	ApiInvoker apiInvoker = ApiInvoker.getInstance();

	public ApiInvoker getInvoker() {
		return apiInvoker;
	}

	public void setBasePath(String basePath) {
		this.basePath = basePath;
	}

	public String getBasePath() {
		return basePath;
	}

	/**
	 * capabilities capabilities
	 * 
	 * @return List<ServiceProviderInfo>
	 */
	public List<ServiceProviderInfo> capabilities() throws ApiException {
		Object postBody = null;

		// create path and map variables
		String path = "/capabilities".replaceAll("\\{format\\}", "json");

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = {

		};

		String contentType = contentTypes.length > 0 ? contentTypes[0] : "application/json";

		if (contentType.startsWith("multipart/form-data")) {
			boolean hasFields = false;
			FormDataMultiPart mp = new FormDataMultiPart();

			if (hasFields)
				postBody = mp;
		} else {

		}

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET", queryParams, postBody, headerParams,
					formParams, contentType);
			if (response != null) {
				return (List<ServiceProviderInfo>) ApiInvoker.deserialize(response, "array", ServiceProviderInfo.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			throw ex;
		}
	}

	/**
	 * search search
	 * 
	 * @param query       The scientific name to search for. For example:
	 *                    \&quot;Bellis perennis\&quot;, \&quot;Prionus\&quot; or
	 *                    \&quot;Bolinus brandaris\&quot;. This is an exact search
	 *                    so wildcard characters are not supported.
	 * @param providers   A list of provider id strings concatenated by comma
	 *                    characters. The default :
	 *                    \&quot;pesi,bgbm-cdm-server[col]\&quot; will be used if
	 *                    this parameter is not set. A list of all available
	 *                    provider ids can be obtained from the
	 *                    &#39;/capabilities&#39; service end point. Providers can
	 *                    be nested, that is a parent provider can have sub
	 *                    providers. If the id of the parent provider is supplied
	 *                    all subproviders will be queried. The query can also be
	 *                    restriced to one or more subproviders by using the
	 *                    following syntax: parent-id[sub-id-1,sub-id2,...]
	 * @param searchMode  Specifies the searchMode. Possible search modes are:
	 *                    scientificNameExact, scientificNameLike (begins with),
	 *                    vernacularNameExact, vernacularNameLike (contains),
	 *                    findByIdentifier. If the a provider does not support the
	 *                    chosen searchMode it will be skipped and the status
	 *                    message in the tnrClientStatus will be set to
	 *                    &#39;unsupported search mode&#39; in this case.
	 * @param addSynonymy Indicates whether the synonymy of the accepted taxon
	 *                    should be included into the response. Turning this option
	 *                    on may cause an increased response time.
	 * @param timeout     The maximum of milliseconds to wait for responses from any
	 *                    of the providers. If the timeout is exceeded the service
	 *                    will jut return the resonses that have been received so
	 *                    far. The default timeout is 0 ms (wait for ever)
	 * @return TnrMsg
	 */
	public TnrMsg search(String query, String providers, String searchMode, Boolean addSynonymy, Long timeout)
			throws ApiException {
		Object postBody = null;

		// create path and map variables
		String path = "/search".replaceAll("\\{format\\}", "json");

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		Map<String, String> formParams = new HashMap<String, String>();

		if (query != null)
			queryParams.put("query", ApiInvoker.parameterToString(query));
		if (providers != null)
			queryParams.put("providers", ApiInvoker.parameterToString(providers));
		if (searchMode != null)
			queryParams.put("searchMode", ApiInvoker.parameterToString(searchMode));
		if (addSynonymy != null)
			queryParams.put("addSynonymy", ApiInvoker.parameterToString(addSynonymy));
		if (timeout != null)
			queryParams.put("timeout", ApiInvoker.parameterToString(timeout));

		String[] contentTypes = {

		};

		String contentType = contentTypes.length > 0 ? contentTypes[0] : "application/json";

		if (contentType.startsWith("multipart/form-data")) {
			boolean hasFields = false;
			FormDataMultiPart mp = new FormDataMultiPart();

			if (hasFields)
				postBody = mp;
		} else {

		}

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET", queryParams, postBody, headerParams,
					formParams, contentType);
			if (response != null) {
				return (TnrMsg) ApiInvoker.deserialize(response, "", TnrMsg.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			throw ex;
		}
	}

}
