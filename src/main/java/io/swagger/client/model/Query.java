package io.swagger.client.model;

import io.swagger.client.model.ClientStatus;
import io.swagger.client.model.Response;
import io.swagger.client.model.Request;
import java.util.*;

import com.wordnik.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * 
 **/
@ApiModel(description = "")
public class Query  {
  
  private List<ClientStatus> clientStatus = new ArrayList<ClientStatus>() ;
  private Request request = null;
  private List<Response> response = new ArrayList<Response>() ;

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("clientStatus")
  public List<ClientStatus> getClientStatus() {
    return clientStatus;
  }
  public void setClientStatus(List<ClientStatus> clientStatus) {
    this.clientStatus = clientStatus;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("request")
  public Request getRequest() {
    return request;
  }
  public void setRequest(Request request) {
    this.request = request;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("response")
  public List<Response> getResponse() {
    return response;
  }
  public void setResponse(List<Response> response) {
    this.response = response;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class Query {\n");
    
    sb.append("  clientStatus: ").append(clientStatus).append("\n");
    sb.append("  request: ").append(request).append("\n");
    sb.append("  response: ").append(response).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
