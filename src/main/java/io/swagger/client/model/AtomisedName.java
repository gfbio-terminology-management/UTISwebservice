package io.swagger.client.model;


import com.wordnik.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * 
 **/
@ApiModel(description = "")
public class AtomisedName  {
  
  private String genusOrUninomial = null;
  private String infragenericEpithet = null;
  private String specificEpithet = null;
  private String infraspecificEpithet = null;

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("genusOrUninomial")
  public String getGenusOrUninomial() {
    return genusOrUninomial;
  }
  public void setGenusOrUninomial(String genusOrUninomial) {
    this.genusOrUninomial = genusOrUninomial;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("infragenericEpithet")
  public String getInfragenericEpithet() {
    return infragenericEpithet;
  }
  public void setInfragenericEpithet(String infragenericEpithet) {
    this.infragenericEpithet = infragenericEpithet;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("specificEpithet")
  public String getSpecificEpithet() {
    return specificEpithet;
  }
  public void setSpecificEpithet(String specificEpithet) {
    this.specificEpithet = specificEpithet;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("infraspecificEpithet")
  public String getInfraspecificEpithet() {
    return infraspecificEpithet;
  }
  public void setInfraspecificEpithet(String infraspecificEpithet) {
    this.infraspecificEpithet = infraspecificEpithet;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class AtomisedName {\n");
    
    sb.append("  genusOrUninomial: ").append(genusOrUninomial).append("\n");
    sb.append("  infragenericEpithet: ").append(infragenericEpithet).append("\n");
    sb.append("  specificEpithet: ").append(specificEpithet).append("\n");
    sb.append("  infraspecificEpithet: ").append(infraspecificEpithet).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
