package io.swagger.client.model;

import io.swagger.client.model.Query;
import java.util.*;

import com.wordnik.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * 
 **/
@ApiModel(description = "")
public class TnrMsg  {
  
  private List<Query> query = new ArrayList<Query>() ;

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("query")
  public List<Query> getQuery() {
    return query;
  }
  public void setQuery(List<Query> query) {
    this.query = query;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class TnrMsg {\n");
    
    sb.append("  query: ").append(query).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
