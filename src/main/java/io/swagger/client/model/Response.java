package io.swagger.client.model;

import io.swagger.client.model.Synonym;
import io.swagger.client.model.Taxon;
import java.util.*;
import io.swagger.client.model.OtherNames;

import com.wordnik.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * 
 **/
@ApiModel(description = "")
public class Response  {
  
  private String checklist = null;
  private List<Synonym> synonyms = new ArrayList<Synonym>() ;
  private List<OtherNames> otherNames = new ArrayList<OtherNames>() ;
  private List<String> vernacularNames = new ArrayList<String>() ;
  private String checklistId = null;
  private String checklistUrl = null;
  private String checklistVersion = null;
  private Taxon taxon = null;
  private String checklistCitation = null;
  private String matchingNameString = null;
  public enum MatchingNameTypeEnum {
     TAXON,  SYNONYM,  VERNACULAR_NAME,  OTHER_NAME, 
  };
  private MatchingNameTypeEnum matchingNameType = null;

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("checklist")
  public String getChecklist() {
    return checklist;
  }
  public void setChecklist(String checklist) {
    this.checklist = checklist;
  }

  
  /**
   * The list synonyms related to the accepted taxon
   **/
  @ApiModelProperty(value = "The list synonyms related to the accepted taxon")
  @JsonProperty("synonyms")
  public List<Synonym> getSynonyms() {
    return synonyms;
  }
  public void setSynonyms(List<Synonym> synonyms) {
    this.synonyms = synonyms;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("otherNames")
  public List<OtherNames> getOtherNames() {
    return otherNames;
  }
  public void setOtherNames(List<OtherNames> otherNames) {
    this.otherNames = otherNames;
  }

  
  /**
   * A common or vernacular name.
   **/
  @ApiModelProperty(value = "A common or vernacular name.")
  @JsonProperty("vernacularNames")
  public List<String> getVernacularNames() {
    return vernacularNames;
  }
  public void setVernacularNames(List<String> vernacularNames) {
    this.vernacularNames = vernacularNames;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("checklistId")
  public String getChecklistId() {
    return checklistId;
  }
  public void setChecklistId(String checklistId) {
    this.checklistId = checklistId;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("checklistUrl")
  public String getChecklistUrl() {
    return checklistUrl;
  }
  public void setChecklistUrl(String checklistUrl) {
    this.checklistUrl = checklistUrl;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("checklistVersion")
  public String getChecklistVersion() {
    return checklistVersion;
  }
  public void setChecklistVersion(String checklistVersion) {
    this.checklistVersion = checklistVersion;
  }

  
  /**
   * The accepted taxon
   **/
  @ApiModelProperty(value = "The accepted taxon")
  @JsonProperty("taxon")
  public Taxon getTaxon() {
    return taxon;
  }
  public void setTaxon(Taxon taxon) {
    this.taxon = taxon;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("checklistCitation")
  public String getChecklistCitation() {
    return checklistCitation;
  }
  public void setChecklistCitation(String checklistCitation) {
    this.checklistCitation = checklistCitation;
  }

  
  /**
   * Refers to the name string of the accepted taxon, synonym or otherName which was matching the query string
   **/
  @ApiModelProperty(value = "Refers to the name string of the accepted taxon, synonym or otherName which was matching the query string")
  @JsonProperty("matchingNameString")
  public String getMatchingNameString() {
    return matchingNameString;
  }
  public void setMatchingNameString(String matchingNameString) {
    this.matchingNameString = matchingNameString;
  }

  
  /**
   * Reports which of the names was matching the query string:  'taxon', 'synonym', 'vernacularName', or 'otherName'
   **/
  @ApiModelProperty(value = "Reports which of the names was matching the query string:  'taxon', 'synonym', 'vernacularName', or 'otherName'")
  @JsonProperty("matchingNameType")
  public MatchingNameTypeEnum getMatchingNameType() {
    return matchingNameType;
  }
  public void setMatchingNameType(MatchingNameTypeEnum matchingNameType) {
    this.matchingNameType = matchingNameType;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class Response {\n");
    
    sb.append("  checklist: ").append(checklist).append("\n");
    sb.append("  synonyms: ").append(synonyms).append("\n");
    sb.append("  otherNames: ").append(otherNames).append("\n");
    sb.append("  vernacularNames: ").append(vernacularNames).append("\n");
    sb.append("  checklistId: ").append(checklistId).append("\n");
    sb.append("  checklistUrl: ").append(checklistUrl).append("\n");
    sb.append("  checklistVersion: ").append(checklistVersion).append("\n");
    sb.append("  taxon: ").append(taxon).append("\n");
    sb.append("  checklistCitation: ").append(checklistCitation).append("\n");
    sb.append("  matchingNameString: ").append(matchingNameString).append("\n");
    sb.append("  matchingNameType: ").append(matchingNameType).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
