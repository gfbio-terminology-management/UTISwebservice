package io.swagger.client.model;

import io.swagger.client.model.ServiceProviderInfo;
import java.util.*;

import com.wordnik.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * 
 **/
@ApiModel(description = "")
public class ServiceProviderInfo  {
  
  private String id = null;
  private String version = null;
  private List<String> searchModes = new ArrayList<String>() ;
  private String label = null;
  private List<ServiceProviderInfo> subChecklists = new ArrayList<ServiceProviderInfo>() ;
  private String documentationUrl = null;
  private String copyrightUrl = null;

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("id")
  public String getId() {
    return id;
  }
  public void setId(String id) {
    this.id = id;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("version")
  public String getVersion() {
    return version;
  }
  public void setVersion(String version) {
    this.version = version;
  }

  
  /**
   * Set of the different SearchModes supported by the service provider and client implementation.Possible search modes are: scientificNameExact, scientificNameLike, vernacularName
   **/
  @ApiModelProperty(value = "Set of the different SearchModes supported by the service provider and client implementation.Possible search modes are: scientificNameExact, scientificNameLike, vernacularName")
  @JsonProperty("searchModes")
  public List<String> getSearchModes() {
    return searchModes;
  }
  public void setSearchModes(List<String> searchModes) {
    this.searchModes = searchModes;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("label")
  public String getLabel() {
    return label;
  }
  public void setLabel(String label) {
    this.label = label;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("subChecklists")
  public List<ServiceProviderInfo> getSubChecklists() {
    return subChecklists;
  }
  public void setSubChecklists(List<ServiceProviderInfo> subChecklists) {
    this.subChecklists = subChecklists;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("documentationUrl")
  public String getDocumentationUrl() {
    return documentationUrl;
  }
  public void setDocumentationUrl(String documentationUrl) {
    this.documentationUrl = documentationUrl;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("copyrightUrl")
  public String getCopyrightUrl() {
    return copyrightUrl;
  }
  public void setCopyrightUrl(String copyrightUrl) {
    this.copyrightUrl = copyrightUrl;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class ServiceProviderInfo {\n");
    
    sb.append("  id: ").append(id).append("\n");
    sb.append("  version: ").append(version).append("\n");
    sb.append("  searchModes: ").append(searchModes).append("\n");
    sb.append("  label: ").append(label).append("\n");
    sb.append("  subChecklists: ").append(subChecklists).append("\n");
    sb.append("  documentationUrl: ").append(documentationUrl).append("\n");
    sb.append("  copyrightUrl: ").append(copyrightUrl).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
