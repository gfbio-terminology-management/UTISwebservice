package io.swagger.client.model;


import com.wordnik.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * 
 **/
@ApiModel(description = "")
public class Classification  {
  
  private String order = null;
  private String family = null;
  private String kingdom = null;
  private String phylum = null;
  private String genus = null;
  private String _class = null;

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("order")
  public String getOrder() {
    return order;
  }
  public void setOrder(String order) {
    this.order = order;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("family")
  public String getFamily() {
    return family;
  }
  public void setFamily(String family) {
    this.family = family;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("kingdom")
  public String getKingdom() {
    return kingdom;
  }
  public void setKingdom(String kingdom) {
    this.kingdom = kingdom;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("phylum")
  public String getPhylum() {
    return phylum;
  }
  public void setPhylum(String phylum) {
    this.phylum = phylum;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("genus")
  public String getGenus() {
    return genus;
  }
  public void setGenus(String genus) {
    this.genus = genus;
  }

  
  /**
   * The full scientific name of the class in which the taxon is classified.
   **/
  @ApiModelProperty(value = "The full scientific name of the class in which the taxon is classified.")
  @JsonProperty("class")
  public String get_Class() {
    return _class;
  }
  public void set_Class(String _class) {
    this._class = _class;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class Classification {\n");
    
    sb.append("  order: ").append(order).append("\n");
    sb.append("  family: ").append(family).append("\n");
    sb.append("  kingdom: ").append(kingdom).append("\n");
    sb.append("  phylum: ").append(phylum).append("\n");
    sb.append("  genus: ").append(genus).append("\n");
    sb.append("  _class: ").append(_class).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
