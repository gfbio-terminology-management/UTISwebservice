package io.swagger.client.model;


import com.wordnik.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * 
 **/
@ApiModel(description = "")
public class ClientStatus  {
  
  private Double duration = null;
  private String checklistId = null;
  private String statusMessage = null;

  
  /**
   * Duration of the request processing in the specific checklist client in milliseconds.
   **/
  @ApiModelProperty(value = "Duration of the request processing in the specific checklist client in milliseconds.")
  @JsonProperty("duration")
  public Double getDuration() {
    return duration;
  }
  public void setDuration(Double duration) {
    this.duration = duration;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("checklistId")
  public String getChecklistId() {
    return checklistId;
  }
  public void setChecklistId(String checklistId) {
    this.checklistId = checklistId;
  }

  
  /**
   * Status of the request, possible values are 'ok', 'timeout', 'interrupted', 'unsupported search mode'.
   **/
  @ApiModelProperty(value = "Status of the request, possible values are 'ok', 'timeout', 'interrupted', 'unsupported search mode'.")
  @JsonProperty("statusMessage")
  public String getStatusMessage() {
    return statusMessage;
  }
  public void setStatusMessage(String statusMessage) {
    this.statusMessage = statusMessage;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class ClientStatus {\n");
    
    sb.append("  duration: ").append(duration).append("\n");
    sb.append("  checklistId: ").append(checklistId).append("\n");
    sb.append("  statusMessage: ").append(statusMessage).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
