package io.swagger.client.model;

import io.swagger.client.model.TaxonName;
import io.swagger.client.model.Source;
import java.util.*;

import com.wordnik.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * 
 **/
@ApiModel(description = "")
public class OtherNames  {
  
  private String url = null;
  private List<Source> sources = new ArrayList<Source>() ;
  private TaxonName taxonName = null;

  
  /**
   * The URL pointing to the original name record of the checklist provider.
   **/
  @ApiModelProperty(value = "The URL pointing to the original name record of the checklist provider.")
  @JsonProperty("url")
  public String getUrl() {
    return url;
  }
  public void setUrl(String url) {
    this.url = url;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("sources")
  public List<Source> getSources() {
    return sources;
  }
  public void setSources(List<Source> sources) {
    this.sources = sources;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("taxonName")
  public TaxonName getTaxonName() {
    return taxonName;
  }
  public void setTaxonName(TaxonName taxonName) {
    this.taxonName = taxonName;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class OtherNames {\n");
    
    sb.append("  url: ").append(url).append("\n");
    sb.append("  sources: ").append(sources).append("\n");
    sb.append("  taxonName: ").append(taxonName).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
