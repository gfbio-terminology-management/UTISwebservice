package io.swagger.client.model;

import io.swagger.client.model.AtomisedName;

import com.wordnik.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * 
 **/
@ApiModel(description = "")
public class TaxonName  {
  
  private String canonicalName = null;
  private AtomisedName atomisedName = null;
  private String fullName = null;
  private String rank = null;
  private String authorship = null;
  private String nomenclaturalReference = null;

  
  /**
   * Canonical name string consisting of only nomenclatural information, i.e. no authorship or taxonomic hierarchy information with the exception of the necessary placements within Genus or Species.
   **/
  @ApiModelProperty(value = "Canonical name string consisting of only nomenclatural information, i.e. no authorship or taxonomic hierarchy information with the exception of the necessary placements within Genus or Species.")
  @JsonProperty("canonicalName")
  public String getCanonicalName() {
    return canonicalName;
  }
  public void setCanonicalName(String canonicalName) {
    this.canonicalName = canonicalName;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("atomisedName")
  public AtomisedName getAtomisedName() {
    return atomisedName;
  }
  public void setAtomisedName(AtomisedName atomisedName) {
    this.atomisedName = atomisedName;
  }

  
  /**
   * The full scientific name, with authorship, publication date information and potentially further taxonomic information.
   **/
  @ApiModelProperty(value = "The full scientific name, with authorship, publication date information and potentially further taxonomic information.")
  @JsonProperty("fullName")
  public String getFullName() {
    return fullName;
  }
  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("rank")
  public String getRank() {
    return rank;
  }
  public void setRank(String rank) {
    this.rank = rank;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("authorship")
  public String getAuthorship() {
    return authorship;
  }
  public void setAuthorship(String authorship) {
    this.authorship = authorship;
  }

  
  /**
   * A reference for the publication in which the scientificName was originally established under the rules of the associated nomenclaturalCode.
   **/
  @ApiModelProperty(value = "A reference for the publication in which the scientificName was originally established under the rules of the associated nomenclaturalCode.")
  @JsonProperty("nomenclaturalReference")
  public String getNomenclaturalReference() {
    return nomenclaturalReference;
  }
  public void setNomenclaturalReference(String nomenclaturalReference) {
    this.nomenclaturalReference = nomenclaturalReference;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class TaxonName {\n");
    
    sb.append("  canonicalName: ").append(canonicalName).append("\n");
    sb.append("  atomisedName: ").append(atomisedName).append("\n");
    sb.append("  fullName: ").append(fullName).append("\n");
    sb.append("  rank: ").append(rank).append("\n");
    sb.append("  authorship: ").append(authorship).append("\n");
    sb.append("  nomenclaturalReference: ").append(nomenclaturalReference).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
